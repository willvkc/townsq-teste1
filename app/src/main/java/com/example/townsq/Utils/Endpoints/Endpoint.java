package com.example.townsq.Utils.Endpoints;

import android.content.Context;

import com.example.townsq.Utils.Dao.Resultado;
import com.example.townsq.Utils.Utilidade;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Endpoint {

    private final Context context;
    private Utilidade utilidade;
    private EndpointCall endpointCall;

    public Endpoint(Context context, EndpointCall endpointCall) {
        this.context = context;
        this.utilidade = new Utilidade(context);
        this.endpointCall = endpointCall;
    }

    public void posts() {
        utilidade.abrirDialog();
        Webservice service = utilidade.getRetrofit(true).create(Webservice.class);
        Call<ArrayList<Resultado>> param = service.posts();
        param.enqueue(new Callback<ArrayList<Resultado>>() {
            @Override
            public void onResponse(Call<ArrayList<Resultado>> call, final Response<ArrayList<Resultado>> response) {
                utilidade.fecharDialog();
                if (response.isSuccessful()) {
                    endpointCall.getResult(response.body(), 0);
                } else {
                    endpointCall.getError("Não foi possível fazer a requisição", 0);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Resultado>> call, Throwable t) {
                utilidade.fecharDialog();
                endpointCall.getError(t.getMessage(), 0);
            }
        });
    }

    public void users(Integer id) {
        utilidade.abrirDialog();
        Webservice service = utilidade.getRetrofit(false).create(Webservice.class);
        Call<Resultado> param = service.users(id);
        param.enqueue(new Callback<Resultado>() {
            @Override
            public void onResponse(Call<Resultado> call, final Response<Resultado> response) {
                utilidade.fecharDialog();
                if (response.isSuccessful()) {
                    ArrayList<Resultado> resultados = new ArrayList<>();
                    resultados.add(response.body());
                    endpointCall.getResult(resultados, 1);
                } else {

                    endpointCall.getError("Não foi possível fazer a requisição", 1);
                }

            }

            @Override
            public void onFailure(Call<Resultado> call, Throwable t) {
                utilidade.fecharDialog();
                utilidade.mostrarToast("Error: " + t.getMessage());
            }
        });
    }


    //Interface para recuperar o resultado das requests
    public interface EndpointCall {
        void getResult(ArrayList<Resultado> resultado, int categoria);
        void getError(String mensagem, int categoria);

    }


}
