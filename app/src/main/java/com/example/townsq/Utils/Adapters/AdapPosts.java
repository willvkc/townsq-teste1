package com.example.townsq.Utils.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.townsq.R;
import com.example.townsq.Utils.Dao.Resultado;
import com.example.townsq.Utils.Utilidade;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by willv on 26/10/2017.
 */

public class AdapPosts extends RecyclerView.Adapter<viewAdapPosts> {

    private ArrayList<Resultado> resultsItemList;
    private AdapPostsCall adapPostsCall;
    private Utilidade utilidade;
    private Context context;

    public AdapPosts(Context context, ArrayList<Resultado> resultsItemList, AdapPostsCall adapPostsCall) {
        this.context = context;
        this.adapPostsCall = adapPostsCall;
        this.resultsItemList = resultsItemList;
        this.utilidade = new Utilidade(context);
    }

    @Override
    public viewAdapPosts onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_post, parent, false);
        viewAdapPosts viewHolder = new viewAdapPosts(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final viewAdapPosts viewHolder, final int position) {
        final Resultado comentariosItem = resultsItemList.get(position);

        viewHolder.titulo.setText(utilidade.tratarNome(comentariosItem.getTitle()));
        viewHolder.texto.setText(utilidade.recortar(comentariosItem.getBody(), 90));
        viewHolder.principal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapPostsCall.abrir(comentariosItem);
            }
        });

    }

    public interface  AdapPostsCall{
        void abrir(Resultado resultado);
    }
    @Override
    public int getItemCount() {
        return resultsItemList.size();
    }
}

//Classe para
class viewAdapPosts extends RecyclerView.ViewHolder {

    @BindView(R.id.titulo)
    TextView titulo;
    @BindView(R.id.texto)
    TextView texto;
    @BindView(R.id.principal)
    LinearLayout principal;

    viewAdapPosts(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }
}

