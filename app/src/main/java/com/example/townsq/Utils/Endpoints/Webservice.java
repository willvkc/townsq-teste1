package com.example.townsq.Utils.Endpoints;




import com.example.townsq.Utils.Dao.Resultado;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface Webservice {

    String URL = "https://jsonplaceholder.typicode.com/";

    @GET("posts/")
    Call<ArrayList<Resultado>> posts();

    @GET("users/{id}")
    Call<Resultado> users(@Path("id") Integer id);



}
