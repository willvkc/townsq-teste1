package com.example.townsq;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.example.townsq.Utils.Adapters.AdapPosts;
import com.example.townsq.Utils.Dao.Resultado;
import com.example.townsq.Utils.Endpoints.Endpoint;
import com.example.townsq.Utils.Utilidade;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements Endpoint.EndpointCall {


    @BindView(R.id.mensagem)
    TextView mensagem;
    @BindView(R.id.lista)
    RecyclerView lista;


    private Context context;
    private Endpoint endpoint;
    private AdapPosts adapPosts;
    private Utilidade utilidade;
    private Resultado resultadoPost;
    private AlertDialog dialogDetalhes;
    private AlertDialog.Builder builder;
    private TextView nome, email, titulo, texto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        context = this;
        utilidade = new Utilidade(context);
        endpoint = new Endpoint(context, this);
        endpoint.posts();


        ActionBar abar = getSupportActionBar();
        View viewActionBar = getLayoutInflater().inflate(R.layout.action_bar, null);
        TextView txtTitle = viewActionBar.findViewById(R.id.txtTitle);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT, Gravity.CENTER);
        txtTitle.setText("Postagens");
        abar.setCustomView(viewActionBar, params);

        abar.setHomeAsUpIndicator(R.drawable.ic_close);
        abar.setDisplayShowCustomEnabled(true);
        abar.setDisplayShowTitleEnabled(false);
        abar.setDisplayHomeAsUpEnabled(true);
        abar.setHomeButtonEnabled(true);
    }


    private void abrir(Resultado autor) {

        if (builder == null) {
            builder = new AlertDialog.Builder(context);
            dialogDetalhes = builder.create();
            dialogDetalhes.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation_up;
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View dialogLayout = inflater.inflate(R.layout.dialog_post, null);
            dialogDetalhes.setView(dialogLayout);
            dialogDetalhes.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            dialogDetalhes.requestWindowFeature(Window.FEATURE_NO_TITLE);
            nome = dialogLayout.findViewById(R.id.nome);
            email = dialogLayout.findViewById(R.id.email);
            titulo = dialogLayout.findViewById(R.id.titulo);
            texto = dialogLayout.findViewById(R.id.texto);
        }

        //post
        titulo.setText(utilidade.tratarNome(resultadoPost.getTitle()));
        texto.setText(resultadoPost.getBody());

        //autor
        nome.setText(utilidade.tratarNome(autor.getName()));
                email.setText(autor.getEmail().toLowerCase() + " / " + autor.getPhone());

        utilidade.abrirDialog(dialogDetalhes);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                finish();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void getResult(ArrayList<Resultado> resultado, int categoria) {

        switch (categoria){
            case 0:
                adapPosts = new AdapPosts(context, resultado, new AdapPosts.AdapPostsCall() {
                    @Override
                    public void abrir(Resultado resultado) {
                        resultadoPost = resultado;
                        endpoint.users(resultado.getUserId());
                    }
                });
                lista.setLayoutManager(new LinearLayoutManager(context));
                lista.setItemAnimator(new DefaultItemAnimator());
                lista.setAdapter(adapPosts);

                break;

            case 1:
                abrir(resultado.get(0));

                break;
        }

    }

    @Override
    public void getError(String mensagem, int categoria) {
        utilidade.mostrarToast(mensagem);
    }
}
